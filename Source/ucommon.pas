unit ucommon;

interface

type
  Common = class
    private
      //
    public
      class function BinarySearch(const inputArray: array of Integer; const inputValue: Integer): Integer;
      class procedure BubbleSort(var inputArray: Array of Integer);
    end;

implementation

class procedure Common.BubbleSort(var inputArray: Array of Integer);
var
  i, temp: Integer;
  changed: Boolean;
begin
  changed := True;

  while changed do
    begin
      changed := False;
      for i := Low(inputArray) to High(inputArray)-1 do
      begin
        if (inputArray[i] > inputArray[i+1]) then
        begin
          temp := inputArray[i+1];
          inputArray[i+1] := inputArray[i];
          inputArray[i] := temp;
          changed := True;
        end;
      end;
    end;
end;

class function Common.BinarySearch(const inputArray: array of Integer; const inputValue: Integer): Integer;
var
  minIndex, maxIndex, midIndex, midValue: Integer;
begin
  minIndex := Low(inputArray);
  maxIndex := High(inputArray);
  while minIndex <= MaxIndex do
    begin
      midIndex := (minIndex + maxIndex) div 2;
      midValue := inputArray[midIndex];
      if inputValue < midValue then
        maxIndex := Pred(midIndex)
      else
        if inputValue = midValue then
          begin
            Result := midIndex;
            Exit; // Successful
          end
        else
          minIndex := Succ(midIndex); // <==> Inc(MidIndex) <==> MidIndex+1
    end;
    Result := -1; // We have not found
end;

end.
