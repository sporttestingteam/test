<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
 <html>
 <body>
   <h2>Input array:</h2>
   <table border="2">
     <tr bgcolor="#00FF7F">
     	<th>value</th>
		<xsl:for-each select="*/*/array[@id='IN_ARRAY']/element">
		<th><xsl:value-of select="value"/></th>
		</xsl:for-each>
     </tr>
	 <tr>
     	<th>index</th>
		<xsl:for-each select="*/*/array[@id='IN_ARRAY']/element">
		<th><xsl:value-of select="index"/></th>
		</xsl:for-each>
     </tr>
   </table>
   <h2>Input value: <xsl:value-of select="//input_value"/></h2>
   <h2>Sorted array:</h2>
    <table border="2">
     <tr bgcolor="#8FBC8F">
     	<th>value</th>
		<xsl:for-each select="*/*/array[@id='SORT_ARRAY']/element">
		<th><xsl:value-of select="value"/></th>
		</xsl:for-each>
     </tr>
	 <tr>
     	<th>index</th>
		<xsl:for-each select="*/*/array[@id='SORT_ARRAY']/element">
		<th><xsl:value-of select="index"/></th>
		</xsl:for-each>
     </tr>
   </table>
   <h2>Result:<xsl:value-of select="//result"/></h2>
   
 </body>
 </html>
</xsl:template>
</xsl:stylesheet>
