object frmMain: TfrmMain
  Left = 317
  Top = 114
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Main'
  ClientHeight = 556
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox2: TGroupBox
    Left = 8
    Top = 8
    Width = 209
    Height = 289
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 7
      Top = 202
      Width = 185
      Height = 81
      TabOrder = 0
      object Label1: TLabel
        Left = 7
        Top = 19
        Width = 102
        Height = 26
        Caption = 'Number of elements  (max. 100)'
        Transparent = True
        WordWrap = True
      end
      object Label3: TLabel
        Left = 7
        Top = 51
        Width = 31
        Height = 13
        Caption = 'Range'
      end
      object edtArraySize: TSpinEdit
        Left = 120
        Top = 16
        Width = 57
        Height = 22
        MaxValue = 100
        MinValue = 2
        TabOrder = 0
        Value = 10
      end
      object edtRange: TSpinEdit
        Left = 120
        Top = 43
        Width = 57
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 100
      end
    end
    object btnMoveUp: TButton
      Left = 136
      Top = 45
      Width = 53
      Height = 33
      Action = acMoveUp
      TabOrder = 1
    end
    object btnMoveDown: TButton
      Left = 136
      Top = 162
      Width = 53
      Height = 33
      Action = acMoveDown
      TabOrder = 2
    end
    object Button1: TButton
      Left = 136
      Top = 125
      Width = 53
      Height = 33
      Action = acRemove
      TabOrder = 3
    end
    object Button2: TButton
      Left = 136
      Top = 85
      Width = 53
      Height = 33
      Action = acClear
      TabOrder = 4
    end
    object lbArrayList: TListBox
      Left = 8
      Top = 42
      Width = 113
      Height = 153
      ItemHeight = 13
      TabOrder = 5
    end
    object btnGenerate: TButton
      Left = 8
      Top = 10
      Width = 97
      Height = 28
      Caption = 'Generate array'
      TabOrder = 6
      OnClick = btnGenerateClick
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 303
    Width = 209
    Height = 94
    Caption = 'Binary Search'
    TabOrder = 1
    object Label2: TLabel
      Left = 17
      Top = 26
      Width = 64
      Height = 13
      Caption = 'Value to find:'
    end
    object edtSearchValue: TEdit
      Left = 86
      Top = 23
      Width = 75
      Height = 21
      TabOrder = 0
      Text = '10'
    end
    object btnSearch: TButton
      Left = 64
      Top = 46
      Width = 97
      Height = 28
      Caption = 'Start Search'
      TabOrder = 1
      OnClick = btnSearchClick
    end
  end
  object Button3: TButton
    Left = 544
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Button3'
    TabOrder = 2
  end
  object PageControl1: TPageControl
    Left = 224
    Top = 16
    Width = 561
    Height = 531
    ActivePage = tsXML
    TabOrder = 3
    object tsXML: TTabSheet
      Caption = 'XML Source'
      object memoXML: TRichEdit
        Left = 0
        Top = 0
        Width = 553
        Height = 503
        Align = alClient
        Lines.Strings = (
          'memoXML')
        TabOrder = 0
      end
    end
    object tsXSLT: TTabSheet
      Caption = 'XSLT Source'
      ImageIndex = 1
      object memoXSLT: TRichEdit
        Left = 0
        Top = 0
        Width = 553
        Height = 503
        Align = alClient
        Lines.Strings = (
          'memoXML')
        TabOrder = 0
      end
    end
    object tsWebBrowser: TTabSheet
      Caption = 'Web Browser'
      ImageIndex = 2
      object wbBrowser: TWebBrowser
        Left = 0
        Top = 0
        Width = 553
        Height = 503
        Align = alClient
        TabOrder = 0
        ControlData = {
          4C000000F2130000FC0E00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
  object GroupBox4: TGroupBox
    Left = 8
    Top = 408
    Width = 209
    Height = 81
    Caption = 'XSLT &Stylesheet'
    TabOrder = 4
    object edtXSLT: TEdit
      Left = 4
      Top = 24
      Width = 193
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
    object btnXSLT: TButton
      Left = 121
      Top = 46
      Width = 75
      Height = 25
      Hint = 'Find an XSLT stylesheet'
      Caption = 'B&rowse...'
      TabOrder = 1
      OnClick = btnXSLTClick
    end
  end
  object GroupBox5: TGroupBox
    Left = 8
    Top = 490
    Width = 208
    Height = 58
    TabOrder = 5
    object btnViewInBrowser: TButton
      Left = 52
      Top = 17
      Width = 104
      Height = 24
      Caption = 'View In Browser'
      TabOrder = 0
      OnClick = btnViewInBrowserClick
    end
  end
  object acMain: TActionList
    Left = 504
    Top = 32
    object acMoveUp: TAction
      Caption = 'Up'
      OnExecute = acMoveUpExecute
      OnUpdate = acMoveUpUpdate
    end
    object acMoveDown: TAction
      Caption = 'Down'
      OnExecute = acMoveDownExecute
      OnUpdate = acMoveDownUpdate
    end
    object acClear: TAction
      Caption = 'Clear'
      OnExecute = acClearExecute
      OnUpdate = acClearUpdate
    end
    object acRemove: TAction
      Caption = 'Remove'
      OnExecute = acRemoveExecute
      OnUpdate = acRemoveUpdate
    end
  end
  object OpenDialog: TOpenDialog
    Left = 160
  end
end
